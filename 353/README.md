# 353-as terem topológiája

A dokumentum magába foglalja a felhasznált hálózati eszközöket, az eszközök bekötését, illetve a hozzáférést az adminisztrációs felülethez.

A jelszavas védelemmel rendelkező eszközök belépési adatai elérhetőek az illetékes személynél.


## Patch panel bekötése
**1**-**13**-ig a fizikai elhelyezkedésünek megfelelően a végpontok kapnak helyet

![Fizikai topológia](https://gitlab.com/Deri-Miksa/network-topology/raw/master/353/images/img1.png)


|  21  |  22  |  23  |  24  |
| :-----------------: | :----------------: | :--------: | :--------: |
| Cisco switch console SW1|Cisco switch console SW2|Cisco router console R1|Déri LAN upstream|


## Hálózati eszközök

![enter image description here](https://gitlab.com/Deri-Miksa/network-topology/raw/master/353/images/img2.png)

 - Cisco Router 3700
	 - 2 DB FastEthernet kimenettel: Fa 0/0 és Fa 0/1
	 - A konzol portja a patch panel 23-as portja
	 - Labor feladatokat lát el
	 - Eszköz neve: R1
 - Cisco Catalyst Switch 3500 XL
	 - 24 DB FastEthernet portja van
	 - 2 DB GBIC portja van 
	 - Alapesetben a VLAN1 felvesz egy IP címet, ha DHCP működik
	 - Labor feladatokat lát el
	 - Eszköz neve: SW2
 - Cisco Catalyst Switch 3500 XL
	 - 24 DB FastEthernet portja van
	 - 2 DB GBIC portja van 
	 - Alapesetben a VLAN1 felvesz egy IP címet, ha DHCP működik
	 - Labor feladatokat lát el
	 - Eszköz neve: SW2
 - TP-Link T[L-SG1024](https://www.tp-link.com/hu/business-networking/unmanaged-switch/tl-sg1024/?utm_medium=select-local)
	- 24 DB 10/100/1000-as portja van
	- Nem menedzselhető
	- Auto MDI / MDIX
	- Alapesetben a 24-es Déri LAN upstream-et osztja szét a végpontok között
 - TP-Link [TL-WR842N](https://www.tp-link.com/hu/home-networking/wifi-router/tl-wr842n/?utm_medium=select-local)
	- 300 Mb/s-os WiFi router
	- WRT: OpenWrt Chaos Calmer 15.05.1
	- Flash: 9.4 MB
	- Memoria: 59.5MB
	- SSIDs:
		- DM-353-intra
		- DM-353-guest

## WiFi Router

Alapesetben a TP-Link saját wrt-je futna az eszközön, viszont mivel emeltebb szintű tűzfalazást és hozzáférés vezérlést szerettünk volna megvalósítani, ezért sokak által ismert OpenWRT firmware-t<sup>[1](#firmware)</sup> telepítattük rá.

Hasznos linkek:
- [OpenWRT telepítési útmutató](https://openwrt.org/toh/tp-link/tl-wr842nd)
- [OEM firmware](https://www.tp-link.com/hu/support/download/tl-wr842n/#Firmware)

A router két 2.4GHz-es vezeték nélküli hálózatot szolgáltat:

|SSID| DM-353-guest | DM-353-intra  |
|:--| :--: | :--: |
|Titkosítás|OPEN|WPA2|
|PSK| Nyílt hálózat | Illetékes személyeknek kiadva |
|Csatorna 2.4GHz| 11 | 11 |
| Hálózati cím | 10.0.1.0/24 | 192.168.1.0/24 |
| Admin panel hozzáférés | letiltva | van |
| SSH hozzáférés | letiltva | van |
| Kliens izoláció<sup>[2](#clientisolation)</sup> | van | nincs |

Router belépés:


*  LuCI URL: `192.168.1.1`
*  Felhasználónév: `root`
*  Jelszó: `Illetékes személyeknek kiadva`

SSH:
* IP: `192.168.1.1`
* Felhasználónév: `root`
* Jelszó: `Illetékes személyeknek kiadva`

___
<a name="firmware">1</a>: A jelenleg működő firmware mellékelve van a repository-ban. 
SHA1: 4c18062db9c996f0929ca71054bbfe23c3de6cd8

<a name="clientisolation">2</a>: A csatlakozott eszközök nem képesek egymás közt adatot forgalmazni, csak a router felé

TODO:
 
* [X]   Forgalom szabályzás a guest hálózaton
* [ ]  p2p és torrent alapú forgalom szűrése a guest hálózaton
* [ ]  Guest figyelmeztető weblap megjelenítése felcsatlakozásnál